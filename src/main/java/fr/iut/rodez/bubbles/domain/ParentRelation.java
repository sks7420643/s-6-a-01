package fr.iut.rodez.bubbles.domain;

public final class ParentRelation extends Relation {

    public ParentRelation(FamilyMember other) {
        super(other);
    }
}
