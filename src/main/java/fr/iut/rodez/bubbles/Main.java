package fr.iut.rodez.bubbles;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.fx.commons.Paints;
import fr.iut.rodez.bubbles.fx.layout.Layout;
import fr.iut.rodez.bubbles.service.FamilyService;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult.Loaded;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult.LoadingError;
import fr.iut.rodez.bubbles.utils.StaticResources;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private static void onLoaded(Stage primaryStage, Family loadedFamily) {
        primaryStage.setTitle(loadedFamily.name());

        Scene scene = new Scene(new Layout(loadedFamily));

        scene.getStylesheets()
             .add(StaticResources.retrieveResourceURL("/styles.css")
                                 .toExternalForm());

        scene.setFill(Paints.BACKGROUND);
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    private static void onLoadingError(Exception cause) {
        System.out.println(cause.getMessage());
    }

    @Override
    public void start(Stage primaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Family File...");
        File file = fileChooser.showOpenDialog(primaryStage);
        switch (FamilyService.loadFamilyFromFile(file)) {
            case Loaded loaded -> onLoaded(primaryStage, loaded.family());
            case LoadingError loadingError -> onLoadingError(loadingError.cause());
        }
    }
}
