package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.commons.Images;
import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.util.function.Consumer;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.EDGE_MANIPULATION;
import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.NODE_MANIPULATION;

public class BottomAppBar extends HBox {

    private Consumer<FamilyGraphResizableCanvas.EventManagementStrategy> onEventManagementStrategyChange;

    public BottomAppBar() {
        setPrefHeight(64);
        setBackground(Backgrounds.GLASS);

        ToggleGroup toggleGroup = new ToggleGroup();

        ToggleButton person = new ToggleButton("Persons", new ImageView(Images.PERSON));
        person.setToggleGroup(toggleGroup);

        ToggleButton relation = new ToggleButton("Relations", new ImageView(Images.RELATION));
        relation.setToggleGroup(toggleGroup);

        toggleGroup.selectToggle(person);

        toggleGroup.selectedToggleProperty()
                   .addListener((observable, oldValue, newValue) -> {
                       if (newValue == person) {
                           onEventManagementStrategyChange.accept(NODE_MANIPULATION);
                       } else if (newValue == relation) {
                           onEventManagementStrategyChange.accept(EDGE_MANIPULATION);
                       } else {
                           toggleGroup.selectToggle(oldValue);
                       }
                   });

        getChildren().addAll(person, relation);
    }

    public void setOnEventManagementStrategyChange(Consumer<FamilyGraphResizableCanvas.EventManagementStrategy> onEventManagementStrategyChange) {
        this.onEventManagementStrategyChange = onEventManagementStrategyChange;
    }
}
