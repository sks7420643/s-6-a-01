package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import javafx.scene.layout.VBox;

import java.util.Collection;

public class FamilyMemberVerticalList extends VBox {

    private static final int DEFAULT_SPACING = 32;

    public FamilyMemberVerticalList(Collection<GraphicalFamilyMember> familyMembers) {
        super(DEFAULT_SPACING, familyMembers.stream()
                                            .map(FamilyMemberBox::new)
                                            .toArray(FamilyMemberBox[]::new));
    }
}
