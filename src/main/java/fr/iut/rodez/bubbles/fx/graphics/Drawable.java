package fr.iut.rodez.bubbles.fx.graphics;

import fr.iut.rodez.bubbles.domain.Position;
import javafx.scene.canvas.GraphicsContext;

public interface Drawable {

    void drawWith(GraphicsContext context);

    boolean covers(Position position);
}
