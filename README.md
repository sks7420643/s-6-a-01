# Bubbles

![bubbles.png](bubbles.png)

## Code of Conduct

When a user wants to add a feature, the flow is as follows:

1. The user creates a new branch from the `main` branch, prefixed by `develop/` and the name of the feature.
2. The user makes the changes in the new branch.
3. The user creates a pull request from the new branch to the `main` branch.
4. The user waits for the pull request to be reviewed and approved by at least one other user.

## Pipeline

The pipeline contains 2 steps (`.gitlab-ci.yml`) :

1. `build` : This step builds the application, creates artifacts (only on the `main` branch).
2. `test` : This step runs the tests of the application.

⚠️: it's highly recommended to run the tests locally before pushing the code.

⚠️: it's highly recommended to run the pipeline before merging the pull request.